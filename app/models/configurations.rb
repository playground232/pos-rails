class Configuration < ApplicationRecord
    self.primary_key = "key"
    validates_presence_of :key
    def self.get(key)
      value = where("key = ?", key).try(:first).try(:value)
      value.present? ? value.to_s : ENV[key.to_s.upcase].to_s
    end
  end
  